export interface Agencia {
    agencia?:       string;
    avatar?:        string;
    departamento?:  string;
    direccion?:     string;
    distrito?:      string;
    favorito?:      boolean;
    favoritoS?:     number;
    id:             string;
    lat:            string | number;
    lon:            string | number;
    provincia?:     string;
}