import { Component, OnInit } from '@angular/core';
import { GlbService } from "../services/glb/glb.service";
import { Agencia } from "../interfaces";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
  agenciasFilt:Agencia[] = [];
  agenciasFind = "";

  constructor(
    private glb: GlbService
  ) { }

  async ngOnInit() {
    await this.glb.init();
    this.filt();
  }

  ionViewWillEnter(){
  }

  ionViewDidEnter() {
    this.glb.isPageLoaded = true;
    this.glb.endLoading();
  }

  filt(){
    const val = this.glb.sinTildes(this.agenciasFind);
    this.agenciasFilt = this.glb.agencias.filter((d) => {
      return (
        this.glb.sinTildes(d.agencia).indexOf(val) !== -1
        || this.glb.sinTildes(d.departamento).indexOf(val) !== -1
        || this.glb.sinTildes(d.distrito).indexOf(val) !== -1
        || this.glb.sinTildes(d.provincia).indexOf(val) !== -1
        || this.glb.sinTildes(d.direccion).indexOf(val) !== -1
        || !val
      )
    })
    this.agenciasFilt.forEach(a => {
      if(a.favorito){a.favoritoS = 1;}else{a.favoritoS = 0;}
    });
    this.agenciasFilt.sort((b, a) => a.favoritoS - b.favoritoS);
  }

  async addFavorito(agencia){
    agencia.favorito=!agencia.favorito;
    await this.glb.storage.save(this.glb.agencias);
  }

}
