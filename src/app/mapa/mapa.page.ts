import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GlbService } from "../services/glb/glb.service";
declare var google;


@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.page.html',
  styleUrls: ['./mapa.page.scss'],
})
export class MapaPage implements OnInit {

  agenciasFilt = [];
  agenciasFind = "";

  map:any;
  initialPosition:any;
  markers = [];

  isModalOpen = false;

  constructor(
    private rotuer: Router,
    private route: ActivatedRoute,
    private glb:GlbService
  ) { }

  async ngOnInit() {
    await this.glb.init();
    this.getMarkers();
    this.loadMap();
    this.filt();
  }

  ionViewDidEnter() {
    this.glb.isPageLoaded = true;
    this.glb.endLoading();
  }

  getMarkers(){
    this.markers = [];
    this.glb.agencias.forEach(agencia => {
      this.markers.push({
        position: {
          lat: +agencia.lon,
          lng: +agencia.lat
        },
        title: agencia.agencia,
        id: agencia.id
      })
    });
    const id = this.route.snapshot.paramMap.get('id');
    const index = this.glb.agencias.findIndex(f => f.id == id);
    this.initialPosition = {
      lat:this.markers[index].position.lat,
      lng:this.markers[index].position.lng
    }
    console.log('markers: ', this.markers);
    console.log('initialPosition: ', this.initialPosition);
  }

  loadMap() {
    const mapEle: HTMLElement = document.getElementById('map');
    this.map = new google.maps.Map(mapEle, {
      center: this.initialPosition,
      zoom: 14
    });
  
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      mapEle.classList.add('show-map');
      this.renderMarkers();
    });

  }

  addMarker(marker:any) {
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      title: marker.title,
      id:marker.id
    });
  }

  renderMarkers() {
    this.markers.forEach(marker => {
      let m = this.addMarker(marker);
      m.addListener("click", () => {
        this.rotuer.navigate(['/agencia',m.id]);
      });
    });
  }

  filt(){
    const val = this.glb.sinTildes(this.agenciasFind);
    this.agenciasFilt = this.glb.agencias.filter((d) => {
      return (
        this.glb.sinTildes(d.agencia).indexOf(val) !== -1
        || this.glb.sinTildes(d.departamento).indexOf(val) !== -1
        || this.glb.sinTildes(d.distrito).indexOf(val) !== -1
        || this.glb.sinTildes(d.provincia).indexOf(val) !== -1
        || this.glb.sinTildes(d.direccion).indexOf(val) !== -1
        || !val
      )
    });
  }

  setOpen(isOpen: boolean) {
    this.isModalOpen = isOpen;
  }

  irMapa(a){
    console.log('agencia',a);
    this.setOpen(false);
    const pos = {
      lat: +a.lon,
      lng: +a.lat
    }
    this.map.panTo(pos,2000);
    /* this.map.setCenter(pos); */
    this.map.setZoom(14)
  }

}
