import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from "@ionic/angular";
import { environment } from "../../../environments/environment.prod";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {


  constructor(
    public storage: Storage,
    public alertController: AlertController,
  ) {
    console.log('hola desde servicio local storage')
    this.initDB()
  }

  async initDB() {
    await this.storage.create();
  }

  async read(k:string) {
    let result: any;
    await this.storage.get(k)
      .then((res) => {
        console.log(`storage read ${k}: `);
        result = res;
      })
      .catch((err) => {
        console.log(`Error storage read ${k}: `, err);
        this.presentAlert('Error', `al leer local storage ${k}`, JSON.stringify(err));
      });
    return result;
  }

  async save(value: any, k: string = environment.KEY_AGENCIAS) {
    let valueString = "";
    if (typeof value === "object") {
      valueString = JSON.stringify(value);
    } else {
      valueString = value;
    }
    await this.storage.set(k, valueString)
      .then((res) => {
        console.log(`Storage save ${k}: `);
      })
      .catch((err) => {
        console.log(`Error Storage save ${k}: `, err);
        this.presentAlert('Error', `al salvar en local storage ${k}`, JSON.stringify(err));
      });
  }

  async presentAlert(titulo,subtitulo,mensaje) {
    const alert = await this.alertController.create({
      mode:'ios',
      header: titulo,
      subHeader: subtitulo,
      message: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }



}
