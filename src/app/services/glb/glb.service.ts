import { Injectable } from '@angular/core';
import { LocalStorageService } from "../localStorage/local-storage.service";
import { ApiService } from "../api/api.service";
import { LoadingController, ToastController } from '@ionic/angular';
import { environment } from "../../../environments/environment.prod";
import { Agencia } from "../../interfaces";

@Injectable({
  providedIn: 'root'
})
export class GlbService {

  loading: any;
  isPageLoaded: boolean = false;
  isDataLoaded: boolean = false;

  agencias:Agencia[] = [];


  constructor(
    public storage: LocalStorageService,
    public api: ApiService,
    public loadingCtrl: LoadingController,
    public toastController: ToastController
  ) {
    console.log('Hola desde servicio global');
  }
  
  async init() {
    this.showLoading();
    await this.getAgencias();
  }

  async showLoading() {
    this.loading = await this.loadingCtrl.create({
      mode: 'ios',
      cssClass: 'loading',
      message: '<img style="height:100%" src="assets/img/logo1.png" alt=""></div><br><ion-spinner name="dots"></ion-spinner>',
      spinner: null
    });
    await this.loading.present();
    this.loading.isLoaded = true;
    this.endLoading();
  }

  endLoading() {
    if (this.loading) {
      if (this.loading.isLoaded && this.isPageLoaded && this.isDataLoaded) {
        this.loadingCtrl.dismiss();
        this.loading = false;
      }
    }
  }

  async presentToast(m:string,color='primry',d=1500,p: 'top' | 'middle' | 'bottom'='bottom') {
    const toast = await this.toastController.create({
      message: m,
      mode: 'ios',
      cssClass: 'toast',
      color: color,
      duration: d,
      position: p
    });
    await toast.present();
  }

  sinTildes(entrada) {
    if (entrada == null) { entrada = ""; }
    let input = entrada.toString();
    var tittles = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç´";
    var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc ";
    for (var i = 0; i < tittles.length; i++) {
        input = input.replace(tittles.charAt(i), original.charAt(i)).toLowerCase().trim();
    };
    return input;
  }

  async getAgencias() {
    if (this.agencias.length == 0) {
      const value = await this.storage.read(environment.KEY_AGENCIAS); /* Busca inicialmente en el local storage */
      if (value) {
        console.log('agencias desde el local storage');
        this.agencias = JSON.parse(value);
      } else {
        console.log('agencias desde DB');
        this.agencias = this.api.getAgencias(); /* si no hay en local storage busca en la DB a través del API */
        await this.storage.save(this.agencias);
        console.log('agencias guardadas')
      }
      console.log('agencias: ', this.agencias);
      this.isDataLoaded = true;
      this.endLoading();
    }
  }

}
