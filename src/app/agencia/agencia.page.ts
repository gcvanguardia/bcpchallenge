import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlbService } from "../services/glb/glb.service";
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-agencia',
  templateUrl: './agencia.page.html',
  styleUrls: ['./agencia.page.scss'],
})
export class AgenciaPage implements OnInit {

  id = '';
  agencia: any;

  agenciaForm: FormGroup;

  constructor(
    private glb: GlbService,
    private route: ActivatedRoute,
    public formBuilder: FormBuilder,
  ) { 
  }

  buidForm() {
    this.agenciaForm = this.formBuilder.group({
      id: [this.agencia.id],
      agencia: [this.agencia.agencia,[Validators.required,Validators.minLength(3)]],
      distrito: [this.agencia.distrito,[Validators.required]],
      provincia: [this.agencia.provincia,[Validators.required]],
      departamento: [this.agencia.departamento,[Validators.required]],
      direccion: [this.agencia.direccion,[Validators.required]],
      avatar: [this.agencia.avatar],
      lat: [this.agencia.lat,[Validators.required]],
      lon: [this.agencia.lon,[Validators.required]]
    });
  }

  async ngOnInit() {
    await this.glb.init();
    await this.getAgencia();
    this.buidForm();
  }
  
  ionViewDidEnter() {
    this.glb.isPageLoaded = true;
    this.glb.endLoading();
  }
  
  async getAgencia() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.agencia = this.glb.agencias.find(f => f.id == this.id);
    console.log('agencia: ', this.agencia);
  }

  onSubmit(e:Event){
    e.preventDefault();
    console.log('formulario: ',this.agenciaForm.value)
    if(this.agenciaForm.valid){
      this.actualizar();
    }else{
      this.agenciaForm.markAllAsTouched();
      this.setFocusErrorForm();
      this.glb.presentToast('Por favor revisa el formulario', 'bcporange');
    }
  }

  setFocusErrorForm(){
    const firstInvalidControl: any = document.querySelector("form .ng-invalid");
    firstInvalidControl.setFocus();
  }

  async actualizar(){
    if(JSON.stringify(this.agencia) === JSON.stringify(this.agenciaForm.value)){
      this.glb.presentToast('Nada para actualizar','primary');
    }else{
      console.log('actualizar');
      await Object.assign(this.agencia,this.agenciaForm.value);
      await this.glb.storage.save(this.glb.agencias); 
      this.glb.presentToast('Agencia actualizada','success');
    }
  }

  /* colorValidate(form: FormGroup, control: string) {
    let color = 'primary';
    if(form.get(control).errors && (form.get(control).dirty || form.get(control).touched)){
      color = 'danger';
    }else if(form.get(control).valid){
      color = 'success';
    } 
    return color;
  } */

}
