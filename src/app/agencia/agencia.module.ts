import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgenciaPageRoutingModule } from './agencia-routing.module';

import { AgenciaPage } from './agencia.page';

import { DefaultValidatorsFormsComponent } from "../components/default-validators-forms/default-validators-forms.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AgenciaPageRoutingModule
  ],
  declarations: [
    AgenciaPage,
    DefaultValidatorsFormsComponent
  ]
})
export class AgenciaPageModule {}
