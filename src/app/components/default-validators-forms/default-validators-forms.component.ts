import { Component, OnInit, Input } from '@angular/core';
import { FormGroup} from '@angular/forms';

@Component({
  selector: 'app-default-validators-forms',
  templateUrl: './default-validators-forms.component.html',
  styleUrls: ['./default-validators-forms.component.scss'],
})
export class DefaultValidatorsFormsComponent implements OnInit {

  @Input() form:FormGroup;
  @Input() control: string;
  @Input() label: string;
  @Input() labelParam: string;

  constructor() { }

  ngOnInit() {}

}
