import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DefaultValidatorsFormsComponent } from './default-validators-forms.component';

describe('DefaultValidatorsFormsComponent', () => {
  let component: DefaultValidatorsFormsComponent;
  let fixture: ComponentFixture<DefaultValidatorsFormsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultValidatorsFormsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DefaultValidatorsFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
